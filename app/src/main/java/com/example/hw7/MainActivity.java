package com.example.hw7;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.method.CharacterPickerDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SimpleDateFormat dateFormat, timeFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        timeFormat = new SimpleDateFormat("HH.mm", Locale.getDefault());

        Button alertDialogWithText = findViewById(R.id.alertDialogWithText);
        alertDialogWithText.setOnClickListener(this);

        Button alertDialogWithList = findViewById(R.id.alertDialogWithList);
        alertDialogWithList.setOnClickListener(this);

        Button alertDialogWithMulti = findViewById(R.id.alertDialogWithMultiChoice);
        alertDialogWithMulti.setOnClickListener(this);

        Button datePickerDialog = findViewById(R.id.datePickerDialog);
        datePickerDialog.setOnClickListener(this);

        Button timePickerDialog = findViewById(R.id.timePickerDialog);
        timePickerDialog.setOnClickListener(this);

        Button progressDialog = findViewById(R.id.progressDialog);
        progressDialog.setOnClickListener(this);

        Button progressHorizontalDialog = findViewById(R.id.progressHorizontalDialog);
        progressHorizontalDialog.setOnClickListener(this);

        Button customDialog = findViewById(R.id.customDialog);
        customDialog.setOnClickListener(this);

        Button firstCustomDialog = findViewById(R.id.firstCustomDialogFragment);
        firstCustomDialog.setOnClickListener(this);

        Button secondCustomDialog = findViewById(R.id.secondCustomDialogFragment);
        secondCustomDialog.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alertDialogWithText:
                showAlertDialogWithText();
                break;
            case R.id.alertDialogWithList:
                showAlertDialogWithList();
                break;
            case R.id.alertDialogWithMultiChoice:
                showAlertDialogWithMulti();
                break;
            case R.id.datePickerDialog:
                showDatePickerDialog();
                break;
            case R.id.customDialog:
                showCustomDialog();
                break;

            case R.id.timePickerDialog:
                showTimePickerDialog();
                break;
            case R.id.progressDialog:
                showProgressDialog();
                break;

            case R.id.progressHorizontalDialog:
                showHorizontalProgressDialog();
                break;

            case R.id.firstCustomDialogFragment:
                showFirstCustomDialogFragment();
                break;

            case R.id.secondCustomDialogFragment:
                showSecondCustomDialogFragment();
                break;
            default:
                Log.d("AlertDialog", "Alert Dialog Log");
        }
    }

    private void showAlertDialogWithText() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Dialog Title");
        builder.setMessage("It's alert dialog with this message");
        builder.setIcon(R.drawable.ic_baseline_add_alert_24);
        //builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNeutralButton("I Dont know", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAlertDialogWithList() {
        String[] countries = {"Kazakhstan", "Russia", "Canada"};
        AlertDialog.Builder builder =  new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Choose your country");
        builder.setItems(countries, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, countries[which], Toast.LENGTH_LONG);
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAlertDialogWithMulti() {
        boolean[] checked = {false, false, false};
        String[] countries = {"Kazakhstan", "Russia", "Canada"};
        AlertDialog.Builder builder =  new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Choose your countries");
        builder.setMultiChoiceItems(
                countries,
                checked,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        checked[which] = isChecked;
                    }
                }
        );
        builder.setCancelable(false);
        builder.setPositiveButton("Show", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringBuilder s = new StringBuilder();
                for(int i =0; i < checked.length; i++){
                    if(checked[i]){
                        s.append(countries[i]).append(" is checked\n");
                    } else {
                        s.append(countries[i] ).append(" is not checked\n");
                    }
                }

                Toast.makeText(
                        MainActivity.this,
                        s.toString(),
                        Toast.LENGTH_LONG
                ).show();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            datePickerDialog = new DatePickerDialog(MainActivity.this);
            datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);

                    Toast.makeText(
                            MainActivity.this,
                            dateFormat.format(calendar.getTime()),
                            Toast.LENGTH_LONG
                    ).show();
                }
            });
        }
        Calendar c = Calendar.getInstance();
        c.set(2020,10,1);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        c.set(2020,10,10);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    private void showTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();

        TimePickerDialog dialog = new TimePickerDialog(
                MainActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(0,0,0,hourOfDay, minute);
                        Toast.makeText(
                                MainActivity.this,
                                timeFormat.format(calendar.getTime()),
                                Toast.LENGTH_LONG
                        ).show();
                    }

                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
                );

        dialog.show();

    }
      private void showProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Downloading...");
        //dialog.setCancelable(false);
        dialog.show();
    }

    private void showHorizontalProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setMessage("Downloading...");
        dialog.setMax(200);
        //dialog.setCancelable(false);
        dialog.show();
        dialog.setProgress(10);
    }

    private void showCustomDialog() {
        View view = LayoutInflater.from(MainActivity.this)
                .inflate(R.layout.layout_dialog, null);

        ImageView  image = findViewById(R.id.dialogImageView);
        TextView text = findViewById(R.id.dialogTextView);
        //image.setImageResource(R.drawable.ic_baseline_add_alert_24);
        //text.setText("I changed text");

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(view);
        builder.setTitle("Custom Dialog");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showFirstCustomDialogFragment(){
        FirstDialogFragment dialog = new FirstDialogFragment();
        dialog.show(getSupportFragmentManager(), "First dialog");
    }

    private void showSecondCustomDialogFragment(){

        SecondDialogFragment dialog = new SecondDialogFragment();
        dialog.show(getSupportFragmentManager(), "Second Dialog");
    }


}